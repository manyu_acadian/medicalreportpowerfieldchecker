
#param ([string] $path)

$path = './data'
$writePath = './jsonData'
'adsfasd.xml' | sls -AllMatches -Pattern '(.*).xml' | % -MemberName Matches | % { $_.Groups[1].Value }

$folder = Get-ChildItem -Path $path
foreach ($file in $folder) {
  $name = $file.Name | sls -AllMatches -Pattern '(.*).xml' | % -MemberName Matches | % { $_.Groups[1].Value };
  [xml]$xml = $file | Get-Content;
  $xml | select -ExpandProperty HealthEMSExport | ConvertTo-Json > "$writePath\\$name.json"
}
#Write-Output $folder
$x = 0
