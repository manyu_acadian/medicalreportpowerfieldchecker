# param (
#   [Parameter(Mandatory = $true)]
#   [string] $AllRunIds,
#   [Parameter(Mandatory = $true)]
#   [string] $xmlFiles
# )

# RUNREPORTS required
# RUNREPORT 0
# POWERFIELDS_.* 0
## POWERFIELD_.* 0
function Get-RunIds {
  param (
    [string] $pFieldRoot,
    [string] $pFieldChild,
    [xml] $xmlDoc
  )
  [string[]]$listOfRunIds = $xmlDoc | Select-Object -ExpandProperty "HealthEMSExport" | Select-Object -ExpandProperty RUNREPORTS | ForEach-Object -MemberName RUNREPORT -ErrorAction "Ignore" | Select-Object -ExpandProperty "$pFieldRoot" -ErrorAction "Ignore" | ForEach-Object -MemberName "$pFieldChild" -ErrorAction "Ignore" | Select-Object -ExpandProperty RUNID -ErrorAction "Ignore" | Select-Object -Unique -ErrorAction "Ignore";
  return $listOfRunIds
}

function Get-SqlRunIds {
  param (
    [string] $file
  )
  $sqlRunIdList = Get-Content $file | ForEach-Object { $_.Trim() } | Where-Object { $_ -ne '' };
  ### Parsing of input BEGIN
  # needs to convert the list of run ids fed in from input. the list should be around 300k-400k in size
  $sqlRunIdSet = New-Object System.Collections.Generic.HashSet[string];
  foreach ($ele in $sqlRunIdList) {
    $sqlRunIdSet.Add($ele) > $null;
  }
  return $sqlRunIdSet
}

function XmlFileAction {
  param (
    [System.IO.FileInfo] $xmlFile,
    [System.Collections.Generic.HashSet[string]] $sqlRunIdSet
  )
  $powerList = @(
    [pscustomobject]@{root = "POWERFIELDS"; child = "POWERFIELD" },
    [pscustomobject]@{root = "POWERFIELDS_CHOICE_USER"; child = "POWERFIELD_CHOICE_USER" },
    [pscustomobject]@{root = "POWERFIELDS_CHOICE_USER_MULT"; child = "POWERFIELD_CHOICE_USER_MULT" },
    [pscustomobject]@{root = "POWERFIELDS_CHOICE_MT"; child = "POWERFIELD_CHOICE_MT" },
    [pscustomobject]@{root = "POWERFIELDS_CHOICE_MT_MULTI"; child = "POWERFIELD_CHOICE_MT_MULTI" }
  )
  # xml parse each file (if failed to parse, should we ignore?)
  # accumulate all of the run ids into a list and once done accumulating.
  #     compare them all to the set from sql
  [string[]]$listOfRunIds = @()
  foreach ($ele in $powerList) {
    # get all run ids from each powerfield type
    $root = $ele.root
    $child = $ele.child
    [xml]$xmlDoc = Get-Content $xmlFile.FullName
    $nextRunIds = Get-RunIds -pFieldRoot $root -pFieldChild $child -xmlDoc $xmlDoc
    $listOfRunIds = $listOfRunIds + $nextRunIds
  }
  $listOfRunIds | ForEach-Object { $_.Trim() } | Where-Object { $_ -ne '' }
  # if there are any run ids in the xml file that do not occur in sql
  #     then do something with that file. (move it, or record it to a file)
  $notFoundRunIds = @()
  $notFoundRunIds = $listOfRunIds | Where-Object { -not ($sqlRunIdSet.Contains($_)) } | Where-Object { $_ -ne '' }
  # if there are run ids not found in the sql set, then the file has not be processed
  # we need to move the file to a different directory
  if (($notFoundRunIds.Length) -ne 0) {
    $path = "$($xmlFile.Directory)\\..\\FilesToProcess"
    if (-not (Test-Path $path)) {
      New-Item -Path $path -ItemType directory
    }
    Move-Item -Path $xmlFile.FullName -Destination "$path\\$($xmlFile.Name)"
    return $true
  }
  else {
    return $false
  }
}

function Main {
  param ([string] $AllRunIds, [string]$xmlFilesD)
  $xmlFiles = Get-ChildItem -path $xmlFilesD;

  $sqlRunIdSet = Get-SqlRunIds -file $AllRunIds;

  # Loop through whole xml folder
  foreach ($xmlFile in $xmlFiles) {
    XmlFileAction -xmlFile $xmlFile -sqlRunIdSet $sqlRunIdSet > $null
  }
}
Main -AllRunIds "$PSScriptRoot/../runIds.txt" -xmlFilesD "$PSScriptRoot/../data/MissingRunIdsExample"
# Main -AllRunIds "$PSScriptRoot/../runIds.txt" -xmlFilesD "$PSScriptRoot/../data/ExistingRunIdsExample"